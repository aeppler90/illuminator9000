/******************************************************************************
*                                                                             *
*   PROJECT : EOS Digital Software Development Kit EDSDK                      *
*      NAME : DownloadCommand.h	                                              *
*                                                                             *
*   Description: This is the Sample code to show the usage of EDSDK.          *
*                                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*   Written and developed by Camera Design Dept.53                            *
*   Copyright Canon Inc. 2006-2008 All Rights Reserved                        *
*                                                                             *
*******************************************************************************/

#pragma once

#include "Command.h"
#include "CameraEvent.h"
#include "EDSDK.h"

class DownloadCommand : public Command
{
private:
	//EdsDirectoryItemRef _directoryItem;

public:
	//DownloadCommand(CameraModel *model, EdsDirectoryItemRef dirItem) : _directoryItem(dirItem), Command(model){}
	DownloadCommand(std::tr1::shared_ptr<CameraModel> model, EdsDirectoryItemRef dirItem, const std::string& folder = "") : m_directroyItem(dirItem), Command(model), m_folder(folder) {}


	virtual ~DownloadCommand()
	{
		//Release item
		if(m_directroyItem != NULL)
		{
			EdsRelease(m_directroyItem);
			m_directroyItem = NULL;
		}
	}


	// Execute command 	
	virtual bool execute()
	{
		EdsError				err = EDS_ERR_OK;
		EdsStreamRef			stream = NULL;

		//Acquisition of the downloaded image information
		EdsDirectoryItemInfo	dirItemInfo;
		err = EdsGetDirectoryItemInfo( m_directroyItem, &dirItemInfo);

		m_filepath = m_folder + (m_folder.empty() ? "" : "/") + std::string(dirItemInfo.szFileName);
	
		// Forwarding beginning notification	
		if(err == EDS_ERR_OK)
		{
			CameraEvent e("DownloadStart");
			_model->notifyObservers(&e);
		}

		//Make the file stream at the forwarding destination
		if(err == EDS_ERR_OK)
		{	
			//err = EdsCreateFileStream(dirItemInfo.szFileName, kEdsFileCreateDisposition_CreateAlways, kEdsAccess_ReadWrite, &stream);
			err = EdsCreateFileStream(m_filepath.c_str(), kEdsFileCreateDisposition_CreateAlways, kEdsAccess_ReadWrite, &stream);
		}	

		//Set Progress
		if(err == EDS_ERR_OK)
		{
			err = EdsSetProgressCallback(stream, ProgressFunc, kEdsProgressOption_Periodically, this);
		}


		//Download image
		if(err == EDS_ERR_OK)
		{
			err = EdsDownload( m_directroyItem, dirItemInfo.size, stream);
		}

		//Forwarding completion
		if(err == EDS_ERR_OK)
		{
			err = EdsDownloadComplete(m_directroyItem);
		}

		//Release Item
		if(m_directroyItem != NULL)
		{
			err = EdsRelease(m_directroyItem);
			m_directroyItem = NULL;
		}

		//Release stream
		if(stream != NULL)
		{
			err = EdsRelease(stream);
			stream = NULL;
		}		
		
		// Forwarding completion notification
		if( err == EDS_ERR_OK)
		{
			CameraEvent e("DownloadComplete", &m_filepath);
			_model->notifyObservers(&e);
		}

		//Notification of error
		if( err != EDS_ERR_OK)
		{
			CameraEvent e("error", &err);
			_model->notifyObservers(&e);
		}

		return true;
	}

private:
	static EdsError EDSCALLBACK ProgressFunc (
						EdsUInt32	inPercent,
						EdsVoid *	inContext,
						EdsBool	*	outCancel
						)
	{
		Command *command = (Command *)inContext;
		CameraEvent e("ProgressReport", &inPercent);
		command->getCameraModel()->notifyObservers(&e);
		return EDS_ERR_OK;
	}

	EdsDirectoryItemRef m_directroyItem;
	std::string			m_folder;
	std::string			m_filepath;


};