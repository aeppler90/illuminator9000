#pragma once

#include "QtSerialPort/qserialport.h"
#include "QtCore/qtextstream.h"

#include <iostream>
#include <string>

#include "SignalConrtol.h"

class TurnttableControl
{
public:

	bool initialze();
	void deinitialze();
	bool turn(float degree);
	TurnttableControl();
	~TurnttableControl();

	bool waitPositionReached();
	void setComPort(std::string port);

	void setSignalControl(SignalConrtol *signalControl);

private:
	QSerialPort m_port;
	QTextStream m_in{ stdin };
	QTextStream m_out{ stdout };

	std::string m_comPort;
	
	SignalConrtol *m_signalControl;
};

