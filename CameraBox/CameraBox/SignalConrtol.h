#pragma once

#include "qobject.h"

class SignalConrtol : public QObject
{
	Q_OBJECT

signals:
	void on_downloadComplet(const QString&);
	void on_postionReached();

public:
	SignalConrtol();
	~SignalConrtol();

	void downloadComplet(const std::string&);
	void postion_reached();
};

