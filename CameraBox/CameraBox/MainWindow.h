#pragma once

#include <QMainWindow>
#include <qfilesystemwatcher.h>

#include "SignalConrtol.h"

class QPushButton;
class QFileDialog;
class QLineEdit;
class QSpinBox;
class QPixmap;
class QLabel;

class MyCameraControl;
class TurnttableControl;

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow();
	~MainWindow();

private slots:
	void getPath();
	void captureSingelImage();
	void startScan();
	void setComPort();
	void init();
	void showImage(const QString&);
	void positionReached();

private:
	SignalConrtol *m_signalConrol;
	void loadImage(QString most_recent_file);

	int m_number_of_images;
	double m_angel;

	MyCameraControl *m_cameraControl;
	TurnttableControl *m_TTControl;

	QWidget *mainWidget;

	QPushButton *m_pushButton_singelImage;
	QPushButton *m_pushButton_startScan;
	QPushButton *m_pushButton_choosePath;
	QPushButton *m_pushButton_initialize;

	QLineEdit *m_lineEdit_comPort;
	QLineEdit *m_lineEdit_path;

	QSpinBox *m_spinBox_rotations;

	QPixmap *m_image;
	QLabel *m_qlabel_image;

	QString m_path;

	QFileSystemWatcher * m_fileSystemWatcher;
};

