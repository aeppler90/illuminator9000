#pragma once 

#include "EDSDK.h"
#include "CameraController.h"

#include <iostream>

bool pictureTaken;

class CameraEventListener
{
public:
	static EdsError EDSCALLBACK  handleObjectEvent (
						EdsUInt32			inEvent,
						EdsBaseRef			inRef,
						EdsVoid *			inContext				
						)
	{

		CameraController*	controller = (CameraController *)inContext;
		std::cout << "EventListener - handleObjectEvent: " << inEvent << std::endl;
		switch(inEvent)
		{
		case kEdsObjectEvent_DirItemRequestTransfer:
			fireEvent(controller, "download", inRef);
			break;
		case 516:
			fireEvent(controller, "download", inRef);
			break;
		default:
			//Object without the necessity is released
			if(inRef != NULL)
			{
				EdsRelease(inRef);
			}
            break;
		}
		//pictureTaken = true;
		return EDS_ERR_OK;		
	}	

	static EdsError EDSCALLBACK  handlePropertyEvent (
						EdsUInt32			inEvent,
						EdsUInt32			inPropertyID,
						EdsUInt32			inParam, 
						EdsVoid *			inContext				
						)
	{

		CameraController*	controller = (CameraController *)inContext;

		switch(inEvent)
		{
		case kEdsPropertyEvent_PropertyChanged:
				fireEvent(controller, "get_Property", &inPropertyID);
				break;

		case kEdsPropertyEvent_PropertyDescChanged:
				fireEvent(controller, "get_PropertyDesc", &inPropertyID);
				break;
		}

		return EDS_ERR_OK;		
	}	

	static EdsError EDSCALLBACK  handleStateEvent (
						EdsUInt32			inEvent,
						EdsUInt32			inParam, 
						EdsVoid *			inContext				
						)
	{

		CameraController*	controller = (CameraController *)inContext;

		switch(inEvent)
		{
		case kEdsStateEvent_Shutdown:
				fireEvent(controller, "shutDown");
				break;
		}

		return EDS_ERR_OK;		
	}	

	static void windowsMessageLoop() {

		HWND hwndMain = NULL;
		HWND hwndDlgModeless = NULL;
		HACCEL haccel = NULL;
		MSG msg;
		BOOL bRet;

		pictureTaken = false;
		//m_pictureTaken = false;
		while ((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
		{
			if (pictureTaken || bRet == -1)
			{
				break;
			}
			else
			{
				if (&msg != NULL) {
					std::cout << "MSG: " << &msg << std::endl;
				}

				if (hwndDlgModeless == (HWND)NULL ||
					!IsDialogMessage(hwndDlgModeless, &msg) &&
					!TranslateAccelerator(hwndMain, haccel,
						&msg))
				{

					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				Sleep(1);
			}
		}
	}

	static void setPictureTaken(bool taken) {
		pictureTaken = taken;
	}

private:
	static void fireEvent(ActionListener *listener, std::string command, void* arg = 0)
	{
		ActionEvent event(command, arg);
		listener->actionPerformed(event);
	}

};
