#include "MainWindow.h"

#include <QtWidgets>
#include <QPushButton>
#include <qfiledialog.h>

#include <iostream>

#include "MyCameraControl.h"
#include "TurnttableControl.h"

MainWindow::MainWindow()
{
	m_path = "./images";
	m_fileSystemWatcher = new QFileSystemWatcher(this);
	m_fileSystemWatcher->addPath(m_path);

	m_signalConrol = new SignalConrtol;

	m_number_of_images = 0;
	m_angel = 0;

	//------------
	//SetUp GUI
	//------------
	mainWidget = new QWidget();
	this->setCentralWidget(mainWidget);

	m_pushButton_singelImage = new QPushButton("Capture Image");
	m_pushButton_startScan = new QPushButton("Start Scan");
	m_pushButton_choosePath = new QPushButton("Set Download Directory");
	m_pushButton_initialize = new QPushButton("Initialize");

	m_pushButton_singelImage->setEnabled(false);
	m_pushButton_startScan->setEnabled(false);

	m_lineEdit_comPort = new QLineEdit("COM8");

	QFormLayout *layoutForm = new QFormLayout();
	layoutForm->addRow(new QLabel(tr("Come Port:")), m_lineEdit_comPort);

	QBoxLayout *main_layout = new QBoxLayout(QBoxLayout::TopToBottom);
	QVBoxLayout *image_layout = new QVBoxLayout();
	QHBoxLayout *bottom_layout = new QHBoxLayout();

	main_layout->addLayout(image_layout);
	main_layout->addLayout(bottom_layout);

	QGroupBox *qGroup_image = new QGroupBox("Image");
	image_layout->addWidget(qGroup_image);
	QGroupBox *qGroup_setup = new QGroupBox("Setup");
	bottom_layout->addWidget(qGroup_setup);
	QGroupBox *qGroup_Controll = new QGroupBox("Control");
	bottom_layout->addWidget(qGroup_Controll);

	m_image = new QPixmap();
	m_qlabel_image = new QLabel();
	m_qlabel_image->setMaximumSize(QSize(900, 600));
	m_qlabel_image->setScaledContents(true);
	m_image->load("C:\\Users\\schur\\Documents\\GitHub\\illuminator9000\\CameraBox\\IMG_4901.jpg");
	m_qlabel_image->setPixmap(*m_image);
	QFormLayout *layout_image = new QFormLayout();
	layout_image->addRow(m_qlabel_image);
	qGroup_image->setLayout(layout_image);

	m_lineEdit_path = new QLineEdit();
	m_lineEdit_path->setReadOnly(true);
	m_spinBox_rotations = new QSpinBox;
	m_spinBox_rotations->setRange(1, 360);
	m_spinBox_rotations->setSingleStep(1);
	m_spinBox_rotations->setValue(36);

	QFormLayout *layout_setup = new QFormLayout();
	layout_setup->addRow(m_pushButton_choosePath, m_lineEdit_path);
	layout_setup->addRow(new QLabel(tr("Number of Images:")), m_spinBox_rotations);
	layout_setup->addRow(new QLabel(tr("ComPort:")), m_lineEdit_comPort);
	qGroup_setup->setLayout(layout_setup);

	QFormLayout *layout_controll = new QFormLayout;
	m_pushButton_singelImage->setMinimumWidth(300);
	m_pushButton_startScan->setMinimumWidth(300);
	m_pushButton_initialize->setMinimumWidth(300);
	layout_controll->addRow(m_pushButton_initialize);
	layout_controll->addRow(m_pushButton_singelImage);
	layout_controll->addRow(m_pushButton_startScan);
	qGroup_Controll->setLayout(layout_controll);

	mainWidget->setLayout(main_layout);
	
	connect(m_pushButton_choosePath, SIGNAL(released()), this, SLOT(getPath()));
	connect(m_pushButton_singelImage, SIGNAL(released()), this, SLOT(captureSingelImage()));
	connect(m_pushButton_startScan, SIGNAL(released()), this, SLOT(startScan()));
	connect(m_lineEdit_comPort, SIGNAL(editingFinished()), this, SLOT(setComPort()));
	connect(m_pushButton_initialize, SIGNAL(released()), this, SLOT(init()));
	//connect(m_fileSystemWatcher, SIGNAL(directoryChanged(const QString&)), this, SLOT(showImage(const QString&)));
	connect(m_signalConrol, SIGNAL(on_downloadComplet(const QString&)), this, SLOT(showImage(const QString&)));
	connect(m_signalConrol, SIGNAL(on_postionReached()), this, SLOT(positionReached()));
	//connect(m_fileSystemWatcher, SIGNAL(fileChanged(const QString&)), this, SLOT(showImage(const QString&)));
}


MainWindow::~MainWindow()
{
	m_TTControl->deinitialze();
	m_cameraControl->deinitializeCamera();
}

void MainWindow::getPath() {
	m_path = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly);
	m_cameraControl->setDownloadFolder(m_path.toUtf8().constData());
	m_lineEdit_path->setText(m_path);
	std::cout << "Download directory set to: " << m_path.toUtf8().constData() << std::endl;
}

void MainWindow::captureSingelImage() {

	m_number_of_images = 0;
	m_angel = 0;

	m_cameraControl->takePicture();
}

void MainWindow::startScan() {

	m_number_of_images = m_spinBox_rotations->value();
	m_angel = 360 / m_number_of_images;

	m_cameraControl->takePicture();
}

void MainWindow::setComPort() {
	m_TTControl->setComPort(m_lineEdit_comPort->text().toUtf8().constData());
}

void MainWindow::init() {
	m_cameraControl = new MyCameraControl;
	m_TTControl = new TurnttableControl;

	m_TTControl->setComPort(m_lineEdit_comPort->text().toUtf8().constData());

	try {

		if (m_cameraControl->initializeCamera()) {
			m_TTControl->initialze();
			m_cameraControl->setSignalControl(m_signalConrol);
			m_TTControl->setSignalControl(m_signalConrol);
			m_cameraControl->setDownloadFolder("./images");

		}
	}
	catch (...) {
		std::cout << "Shutdown error handeled." << std::endl;
	}

	m_pushButton_singelImage->setEnabled(true);
	m_pushButton_startScan->setEnabled(true);
}

void MainWindow::showImage(const QString& path) {
	qDebug() << path;

	bool image_loaded = m_image->load(path);
	m_qlabel_image->setPixmap(*m_image);

	if (m_number_of_images > 0) {
		m_number_of_images--;
		m_TTControl->turn(m_angel);
	}

	//QString most_recent_file = NULL;

	//QDirIterator it(path, QStringList() << "*.jpg", QDir::Files, QDirIterator::Subdirectories);
	//while (it.hasNext()) {
	//	QString current_file = it.next();
	//	qDebug() << current_file;
	//	qDebug() << QFileInfo(current_file).created();

	//	if (most_recent_file == NULL) {
	//		most_recent_file = current_file;
	//	}
	//	else if(QFileInfo(current_file).created() > QFileInfo(most_recent_file).created()) {
	//		most_recent_file = current_file;
	//	}
	//}

	//Sleep(10000);
	//bool image_loaded = m_image->load(QFileInfo(most_recent_file).absoluteFilePath());
	//bool image_loaded = m_image->load("C:\\Users\\schur\\Documents\\GitHub\\illuminator9000\\CameraBox\\IMG_4901.jpg");
	//bool image_loaded = m_image->load("C:\\Users\\schur\\Documents\\GitHub\\illuminator9000\\CameraBox\\IMG_4938.jpg");
	//bool image_loaded = m_image->load("C:\\Users\\schur\\Documents\\GitHub\\illuminator9000\\CameraBox\\IMG_4938.jpg");
	//qDebug() << QFileInfo(most_recent_file).size();
	//m_qlabel_image->setPixmap(*m_image);

}

void MainWindow::loadImage(QString most_recent_file) {

	while (true)
	{
		if (QFileInfo(most_recent_file).size() != 0) {
			bool image_loaded = m_image->load(QFileInfo(most_recent_file).absoluteFilePath());
			m_qlabel_image->setPixmap(*m_image);
		}
		else {
			Sleep(100);
		}

	}

}

void MainWindow::positionReached() {
	m_cameraControl->takePicture();
}
