#pragma once
#pragma warning(disable : 4996)

#include "EDSDK.h"
#include "EDSDKErrors.h"
#include "EDSDKTypes.h"

#include "CameraModel.h"
#include "CameraController.h"
#include "Observer.h"
#include "CameraEvent.h"

#include "SignalConrtol.h"

class MyCameraControl : public Observer
{


public:
	MyCameraControl();
	~MyCameraControl();

	bool initializeCamera();
	bool deinitializeCamera();

	bool takePicture();
	void setDownloadFolder(std::string folder);

	bool m_imageTaken;

	void setSignalControl(SignalConrtol *signalControl);

private:
	bool setEventHandler();

	void update(Observable* from, CameraEvent *e);

	EdsError getFirstCamera();
	void cameraModelFactory(EdsDeviceInfo deviceInfo);

	EdsError m_err = EDS_ERR_OK;
	EdsCameraRef m_camera = NULL;
	std::tr1::shared_ptr<CameraModel> m_camera_model;
	//CameraModel* m_camera_model;
	std::tr1::shared_ptr<CameraController> m_camera_controller;
	bool m_isSDKLoaded = false;

	std::string * image_path;

	SignalConrtol *m_signalControl;
};

