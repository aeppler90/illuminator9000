#include "TurnttableControl.h"

#include <iostream>
#include <windows.h>

#include <thread>


TurnttableControl::TurnttableControl()
{
	m_comPort = "COM8";
}


TurnttableControl::~TurnttableControl()
{
}

bool TurnttableControl::initialze() {
	
	m_port.setPortName(QString::fromStdString(m_comPort)); //if the arduino is not detected change the COM-Port
	m_port.setBaudRate(9600);
	m_port.setDataBits(QSerialPort::Data8);
	m_port.setParity(QSerialPort::NoParity);
	m_port.setStopBits(QSerialPort::OneStop);
	m_port.setFlowControl(QSerialPort::NoFlowControl);

	if (!m_port.open(QSerialPort::ReadWrite)) {
		//m_out << "Error opening serial port: " << m_port.errorString() << endl;
		return false;
	}

	m_port.setDataTerminalReady(true);
	while (true) {

		m_port.waitForReadyRead(3000);

		if (m_port.canReadLine()) {
			QByteArray data_read = m_port.readLine();
			QString dataAsString = QString(data_read);
			std::string s_read = dataAsString.toUtf8().constData();
			std::cout << "Serail Read - Initilaize: " << s_read << std::endl;

			if (s_read == "s\r\n") {
				break;
			}
		}
		else {
			std::cout << "no start by Arduino" << std::endl;
		}
	}

	return true;
}

void TurnttableControl::deinitialze() {
/*	if (m_port.isOpen()) {
		m_port.close();
	}*/		
}

bool TurnttableControl::turn(float degree) {

	QString qs;
	qs = "a" + QString::number(degree);
	QByteArray data(qs.toUtf8());
	//m_port_mutex.lock();

	m_port.write(data);

	return waitPositionReached();

}


bool TurnttableControl::waitPositionReached() {

	m_port.flush();

	while (true) {
		
		m_port.waitForBytesWritten(3000);
		m_port.waitForReadyRead(3000);

		if (m_port.canReadLine()) {
			QByteArray data_read = m_port.readLine();
			QString dataAsString = QString(data_read);
			std::string s_read = dataAsString.toUtf8().constData();
			std::cout << "Serail Read - Position: " << s_read << std::endl;

			if (s_read == "r\r\n") {
				m_signalControl->postion_reached();
				return true;
			}
		}
	}

	return false;
}

void TurnttableControl::setComPort(std::string port) {
	m_comPort = port;
}

void TurnttableControl::setSignalControl(SignalConrtol *signalControl) {
	m_signalControl = signalControl;
}
