#include <iostream>
#include "EDSDK.h"
#include "EDSDKErrors.h"
#include "EDSDKTypes.h"
#include <thread>
#include <chrono>

char * directory = "Here is your directory";

void download_img(EdsBaseRef & object, EdsVoid * & context)
{
	EdsStreamRef stream = NULL;
	EdsDirectoryItemInfo dirItemInfo;
	EdsGetDirectoryItemInfo(object, &dirItemInfo);
	strcat(directory, file_name);
	EdsCreateFileStream(directory, kEdsFileCreateDisposition_CreateAlways, kEdsAccess_ReadWrite, &stream);
	EdsDownload(object, dirItemInfo.size, stream);
	EdsDownloadComplete(object);
	EdsRelease(stream);
	stream = NULL;
	if (object)
		EdsRelease(object);
}

EdsError EDSCALLBACK handleObjectEvent(EdsObjectEvent event, EdsBaseRef object, EdsVoid * context)
{
	download_img(object, context);
	return EDS_ERR_OK;
}

void init_camera(EdsCameraRef & camera)
{
	EdsError err = EDS_ERR_OK;
	EdsCameraListRef cameraList = NULL;
	EdsUInt32 count = 0;
	camera = NULL;

	err = EdsInitializeSDK();
	err = EdsGetCameraList(&cameraList);
	err = EdsGetChildCount(cameraList, &count);
	if (count > 0)
	{
		err = EdsGetChildAtIndex(cameraList, 0, &camera);
		EdsRelease(cameraList);
	}
	EdsSetObjectEventHandler(camera, kEdsObjectEvent_DirItemCreated, handleObjectEvent, NULL);
	EdsSendStatusCommand(camera, kEdsCameraStatusCommand_UIUnLock, 0);
}

void start_liveview(EdsCameraRef camera)
{
	EdsOpenSession(camera);
	EdsUInt32 device = kEdsPropID_Evf_OutputDevice;
	EdsGetPropertyData(camera, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);

	device |= kEdsEvfOutputDevice_PC;
	EdsSetPropertyData(camera, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);
}

void stop_liveview(EdsCameraRef camera)
{
	EdsUInt32 device;
	EdsGetPropertyData(camera, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);
	device &= ~kEdsEvfOutputDevice_PC;
	EdsSetPropertyData(camera, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);
	EdsCloseSession(camera);
}

void take_video(EdsCameraRef camera, int length)
{
	EdsOpenSession(camera);
	EdsUInt32 record_start = 4; // Begin movie shooting 
	EdsSetPropertyData(camera, kEdsPropID_Record, 0, sizeof(record_start), &record_start);
	this_thread::sleep_for(chrono::milliseconds(length));
	EdsUInt32 record_stop = 0; // End movie shooting 
	EdsSetPropertyData(camera, kEdsPropID_Record, 0, sizeof(record_stop), &record_stop);
	EdsCloseSession(camera);
}

void update_data(EdsCameraRef camera)
{
	EdsOpenSession(camera);
	// here event happens
	EdsCloseSession(camera);
}

void take_photo(EdsCameraRef camera, int count, int interv_millsec)
{
	for (int i = 0; i < count; ++i) {
		EdsOpenSession(camera);
		cout << "shoot" << endl;
		EdsSendCommand(camera, kEdsCameraCommand_TakePicture, 0);
		this_thread::sleep_for(chrono::milliseconds(interv_millsec));
		EdsCloseSession(camera);
	}
	update_data(camera);
}

void dispose(EdsCameraRef camera)
{
	EdsCloseSession(camera);
	EdsTerminateSDK();
}

int main() {
	EdsCameraRef camera;
	init_camera(camera);

	take_photo(camera, 1, 10);

	// to take video
	// firstly you have to start liveview
	// secondly call "take video"
	// thirdly stop liveview
	return 0;
}