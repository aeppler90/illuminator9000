// Illuminator9000.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//
#pragma once

#include "pch.h"
#include "EDSDK.h"
#include "EDSDKErrors.h"
#include "EDSDKTypes.h"
#include <iostream>

#include "CameraController.h"
#include "EDSDK.h"
#include "CameraModel.h"
#include "Processor.h"

#include "ActionListener.h"
#include "ActionEvent.h"

#include "OpenSessionCommand.h"
#include "CloseSessionCommand.h"
#include "SaveSettingCommand.h"
#include "TakePictureCommand.h"
#include "DownloadCommand.h"
#include "GetPropertyCommand.h"
#include "GetPropertyDescCommand.h"
#include "SetPropertyCommand.h"
#include "SetCapacityCommand.h"
#include "NotifyCommand.h"

#include "StartEvfCommand.h"
#include "EndEvfCommand.h"
#include "DownloadEvfCommand.h"
#include "DriveLensCommand.h"
#include "PressShutterButtonCommand.h"
#include "GetPropertyCommand.h"
#include "DoEvfAFCommand.h"

#include "CameraModelLegacy.h"

#include "stdafx.h"
#include "CameraControl.h"
#include "CameraControlDlg.h"
#include "EDSDK.h"
#include "EDSDKTypes.h"
#include "CameraModel.h"
#include "CameraModelLegacy.h"
#include "CameraController.h"
#include "CameraEventListener.h"

CameraModel*		_model;
CameraController*	_controller;

EdsError getFirstCamera(EdsCameraRef    *camera)
{
	EdsError err = EDS_ERR_OK;
	EdsCameraListRef    cameraList = NULL;
	EdsUInt32    count = 0;

	// Get camera list 
	err = EdsGetCameraList(&cameraList);

	// Get number of cameras 
	if (err == EDS_ERR_OK)
	{
		err = EdsGetChildCount(cameraList, &count);
		if (count == 0)
		{
			err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}
	// Get first camera retrieved 
	if (err == EDS_ERR_OK)
	{
		err = EdsGetChildAtIndex(cameraList, 0, camera);
	}

	// Release camera list 
	if (cameraList != NULL)
	{
		EdsRelease(cameraList);
		cameraList = NULL;
	}
	return err;
}



EdsError    getTv(EdsCameraRef    camera, EdsUInt32    *Tv)
{
	EdsError err = EDS_ERR_OK;
	EdsDataType dataType;
	EdsUInt32    dataSize;

	err = EdsGetPropertySize(camera, kEdsPropID_Tv, 0, &dataType, &dataSize);

	if (err == EDS_ERR_OK)
	{
		err = EdsGetPropertyData(camera, kEdsPropID_Tv, 0, dataSize, Tv);
	}

	return err;
}

EdsError    getTvDesc(EdsCameraRef    camera, EdsPropertyDesc    *TvDesc)
{
	EdsError err = EDS_ERR_OK;

	err = EdsGetPropertyDesc(camera, kEdsPropID_Tv, TvDesc);

	return err;
}

EdsError    setTv(EdsCameraRef    camera, EdsUInt32    TvValue)
{
	EdsError err = EDS_ERR_OK;

	err = EdsSetPropertyData(camera, kEdsPropID_Tv, 0, sizeof(TvValue), &TvValue);

	return err;
}

EdsError downloadImage(EdsDirectoryItemRef    directoryItem)
{
	EdsError err = EDS_ERR_OK;
	EdsStreamRef stream = NULL;

	// Get directory item information 
	EdsDirectoryItemInfo    dirItemInfo;
	err = EdsGetDirectoryItemInfo(directoryItem, &dirItemInfo);

	// Create file stream for transfer destination 
	if (err == EDS_ERR_OK)
	{
		err = EdsCreateFileStream(dirItemInfo.szFileName,
			kEdsFileCreateDisposition_CreateAlways,
			kEdsAccess_ReadWrite, &stream);
	}

	// Download image 
	if (err == EDS_ERR_OK)
	{
		err = EdsDownload(directoryItem, dirItemInfo.size, stream);
	}

	// Issue notification that download is complete 
	if (err == EDS_ERR_OK)
	{
		err = EdsDownloadComplete(directoryItem);
	}

	// Release stream 
	if (stream != NULL)
	{
		EdsRelease(stream);
		stream = NULL;
	}

	return err;
}

EdsError getDCIMFolder(EdsVolumeRef    volume, EdsDirectoryItemRef *    directoryItem)
{
	EdsError err = EDS_ERR_OK;
	EdsDirectoryItemRef    dirItem = NULL; //Item to download from Camera
	EdsDirectoryItemInfo    dirItemInfo;
	EdsUInt32 count = 0;

	// Get number of items under the volume 
	err = EdsGetChildCount(volume, &count);
	if (err == EDS_ERR_OK && count == 0)
	{
		err = EDS_ERR_DIR_NOT_FOUND;
	}

	// Get DCIM folder 
	for (int i = 0; i < count && err == EDS_ERR_OK; i++)
	{
		// Get the ith item under the specified volume 
		if (err == EDS_ERR_OK)
		{
			err = EdsGetChildAtIndex(volume, i, &dirItem);
		}

		// Get retrieved item information 
		if (err == EDS_ERR_OK)
		{
			err = EdsGetDirectoryItemInfo(dirItem, &dirItemInfo);
		}

		// Indicates whether or not the retrieved item is a DCIM folder. 
		if (err == EDS_ERR_OK)
		{
			if (_stricmp(dirItemInfo.szFileName, "DCIM") == 0 &&
				dirItemInfo.isFolder == true)
			{
				directoryItem = &dirItem;
				break;
			}
		}

		// Release retrieved item 
		if (dirItem)
		{
			EdsRelease(dirItem);
			dirItem = NULL;
		}
	}

	return err;
}

EdsError takePicture(EdsCameraRef camera)
{
	EdsError err;

	err = EdsSendCommand(camera, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_Completely);

	err = EdsSendCommand(camera, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_OFF);
	return err;
}


EdsError getVolume(EdsCameraRef camera, EdsVolumeRef* volume)
{
	EdsError err = EDS_ERR_OK;
	EdsUInt32 count = 0;
	// Get the number of camera volumes 
	err = EdsGetChildCount(camera, &count);
	if (err == EDS_ERR_OK && count == 0)
	{
		err = EDS_ERR_DIR_NOT_FOUND;
	}

	// Get initial volume 
	if (err == EDS_ERR_OK)
	{
		err = EdsGetChildAtIndex(camera, 0, volume);
	}

	return err;
}


EdsError EDSCALLBACK handleObjectEvent(EdsObjectEvent event, EdsBaseRef object, EdsVoid* context)
{
	EdsError err = EDS_ERR_OK;
	std::cout << "Object Handler" << std::endl;
	// do something 



  switch(event)
{
  case kEdsObjectEvent_DirItemRequestTransfer:
	downloadImage(object);
	break;

  default:

	break;
}


// Object must be released 
	if (object)

	{
		EdsRelease(object);
	}
	return err;
}


EdsError EDSCALLBACK handlePropertyEvent(EdsPropertyEvent event, EdsPropertyID property, EdsVoid * context)
{
	EdsError err = EDS_ERR_OK;
	// do something 
	return err;
}

/*EdsError EDSCALLBACK    handleStateEvent(EdsCameraStateEvent event,
	EdsUInt32 parameter,
	EdsVoid * context)
{
	// do something
}*/

void    applicationRun()
{
	EdsError err = EDS_ERR_OK;
	EdsCameraRef    camera = NULL;
	bool isSDKLoaded = false;
	MSG Msg;

	// Initialize SDK 
	err = EdsInitializeSDK();
	if (err == EDS_ERR_OK)
	{
		isSDKLoaded = true;
	}

	// Get first camera 
	if (err == EDS_ERR_OK)
	{
		std::cout << "Checking for Camera" << std::endl;
		// See Sample 2. 
		err = getFirstCamera(&camera);
	}

	// Set Object event handler 
	if (err == EDS_ERR_OK)
	{
		std::cout << "Found Camera" << std::endl;
		err = EdsSetObjectEventHandler(camera, kEdsObjectEvent_All, handleObjectEvent, NULL);
		std::cout << "Err Value for Event: " << err << std::endl;
	}

	// Set Property event handler 
	/*if (err == EDS_ERR_OK)
	{
		err = EdsSetPropertyEventHandler(camera, kEdsPropertyEvent_All,
			handlePropertyEvent, NULL);
	}

	// Set State event handler 
	if (err == EDS_ERR_OK)
	{
		err = EdsSetCameraStateEventHandler(camera, kEdsStateEvent_All,
			handleStateEvent, NULL);
	}*/

	// Open session with camera 
	if (err == EDS_ERR_OK)
	{
		err = EdsOpenSession(camera);
	}

	///// 
		  // do something 
		  //// 
	takePicture(camera);
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if(Msg != NULL){
			std::cout << "MSG: " << &Msg << std::endl;
		}
		
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
		Sleep(1);
	}

	// Close session with camera 
	if (err == EDS_ERR_OK)
	{
		err = EdsCloseSession(camera);
	}

	// Release camera 
	if (camera != NULL)
	{
		EdsRelease(camera);
	}

	// Terminate SDK 
	if (isSDKLoaded)
	{
		EdsTerminateSDK();
	}
}

CameraModel* cameraModelFactory(EdsCameraRef camera, EdsDeviceInfo deviceInfo)
{
	// if Legacy protocol.
	if (deviceInfo.deviceSubType == 0)
	{
		return new CameraModelLegacy(camera);
	}

	// PTP protocol.
	return new CameraModel(camera);
}

BOOL InitInstance()
{
	//InitCommonControls();
	//CWinApp::InitInstance();


	EdsError	 err = EDS_ERR_OK;
	EdsCameraListRef cameraList = NULL;
	EdsCameraRef camera = NULL;
	EdsUInt32	 count = 0;
	bool		 isSDKLoaded = false;

	// Initialization of SDK
	err = EdsInitializeSDK();

	if (err == EDS_ERR_OK)
	{
		isSDKLoaded = true;
	}

	//Acquisition of camera list
	if (err == EDS_ERR_OK)
	{
		err = EdsGetCameraList(&cameraList);
	}

	//Acquisition of number of Cameras
	if (err == EDS_ERR_OK)
	{
		err = EdsGetChildCount(cameraList, &count);
		if (count == 0)
		{
			err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}


	//Acquisition of camera at the head of the list
	if (err == EDS_ERR_OK)
	{
		err = EdsGetChildAtIndex(cameraList, 0, &camera);
	}

	//Acquisition of camera information
	EdsDeviceInfo deviceInfo;
	if (err == EDS_ERR_OK)
	{
		err = EdsGetDeviceInfo(camera, &deviceInfo);
		if (err == EDS_ERR_OK && camera == NULL)
		{
			err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}


	//Release camera list
	if (cameraList != NULL)
	{
		EdsRelease(cameraList);
	}

	//Create Camera model
	if (err == EDS_ERR_OK)
	{
		_model = cameraModelFactory(camera, deviceInfo);
		if (_model == NULL)
		{
			err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}

	if (err != EDS_ERR_OK)
	{
		std::cout << "Cannot detect camera"<< std::endl;
	}

	if (err == EDS_ERR_OK)
	{
		//Create CameraController
		_controller = new CameraController();
		//Create View Dialog
		//CCameraControlDlg view;

		_controller->setCameraModel(_model);
		//_model->addObserver(&view);
		// Send Model Event to view	
		//view.setCameraController(_controller);

		//Set Property Event Handler
		if (err == EDS_ERR_OK)
		{
			err = EdsSetPropertyEventHandler(camera, kEdsPropertyEvent_All, CameraEventListener::handlePropertyEvent, (EdsVoid *)_controller);
		}

		//Set Object Event Handler
		if (err == EDS_ERR_OK)
		{
			err = EdsSetObjectEventHandler(camera, kEdsObjectEvent_All, CameraEventListener::handleObjectEvent, (EdsVoid *)_controller);
		}

		//Set State Event Handler
		if (err == EDS_ERR_OK)
		{
			err = EdsSetCameraStateEventHandler(camera, kEdsStateEvent_All, CameraEventListener::handleStateEvent, (EdsVoid *)_controller);
		}

		//m_pMainWnd = &view;
		//INT_PTR nResponse = view.DoModal();

	}

	//Release Camera
	if (camera != NULL)
	{
		EdsRelease(camera);
		camera = NULL;
	}

	//Termination of SDK
	if (isSDKLoaded)
	{
		EdsTerminateSDK();
	}

	if (_model != NULL)
	{
		delete _model;
		_model = NULL;
	}


	if (_controller != NULL)
	{
		delete _controller;
		_controller = NULL;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int main()
{
	std::cout << "Hello World!\n";
	//EdsError test = EdsInitializeSDK();
	//std::cout << test << std::endl;
	applicationRun();
	system("pause");
}



// Programm ausführen: STRG+F5 oder "Debuggen" > Menü "Ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
