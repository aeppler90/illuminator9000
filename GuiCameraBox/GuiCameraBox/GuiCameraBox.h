#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_GuiCameraBox.h"

class GuiCameraBox : public QMainWindow
{
	Q_OBJECT

public:
	GuiCameraBox(QWidget *parent = Q_NULLPTR);

private:
	Ui::GuiCameraBoxClass ui;
};
