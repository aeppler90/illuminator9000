#include "MyCameraControl.h"

#include "CameraModelLegacy.h"
#include "CameraEventListener.h"

#include <iostream>
#include <stdio.h>
#include <functional>
#include <memory>

#include <CameraController.h>
#include <CameraModel.h>

MyCameraControl::MyCameraControl(){
}


MyCameraControl::~MyCameraControl(){
}

bool MyCameraControl::initializeCamera() {

	// Initialize SDK 
	m_err = EdsInitializeSDK();
	if (m_err == EDS_ERR_OK) {
		m_isSDKLoaded = true;
	}

	// Get first camera 
	if (m_err == EDS_ERR_OK) {
		m_err = getFirstCamera();
	}

	//Acquisition of camera information
	EdsDeviceInfo deviceInfo;
	if (m_err == EDS_ERR_OK)
	{
		m_err = EdsGetDeviceInfo(m_camera, &deviceInfo);
		if (m_err == EDS_ERR_OK && m_camera == NULL)
		{
			m_err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}

	//Create Camera model
	if (m_err == EDS_ERR_OK)
	{
		cameraModelFactory(deviceInfo);
		if (m_camera_model == NULL){
			m_err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}

	if (!setEventHandler()) {
		std::cout << "Failed to set camera event handler." << std::endl;
		return false;
	}

	// Open session with camera 
	if (m_err == EDS_ERR_OK) {
		m_err = EdsOpenSession(m_camera);
	}

	if (m_err != EDS_ERR_OK) {
		std::cout << "Failed connecting with camera." << std::endl;
		return false;
	}
	return true;
}

bool MyCameraControl::deinitializeCamera() {

	// Close session with camera 
	if (m_err == EDS_ERR_OK) {
		m_err = EdsCloseSession(m_camera);
	}

	// Release camera 
	if (m_camera != NULL) {
		EdsRelease(m_camera);
	}

	// Terminate SDK 
	if (m_isSDKLoaded) {
		EdsTerminateSDK();
	}

	if (m_err != EDS_ERR_OK) {
		std::cout << "Failed to deinitialize camera connection." << std::endl;
		return false;
	}
	return true;
}

EdsError EDSCALLBACK handleObjectEvent(EdsUInt32 inEvent, EdsBaseRef inRef, EdsVoid * inContext){

	//CameraController*	controller = (CameraController *)inContext;

	std::cout << "CallBack!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;

	switch (inEvent)
	{
	case kEdsObjectEvent_DirItemRequestTransfer:
		//fireEvent(controller, "download", inRef);
		//setPictureTaken(true);
		break;

	default:
		//Object without the necessity is released
		if (inRef != NULL)
		{
			EdsRelease(inRef);
		}
		break;
	}


	return EDS_ERR_OK;
}

EdsError EDSCALLBACK handlePropertyEvent(EdsPropertyEvent event, EdsPropertyID property, EdsVoid * context) {
	EdsError err = EDS_ERR_OK;
	// do something
	return err;
}

EdsError EDSCALLBACK handleStateEvent(EdsStateEvent event, EdsUInt32 parameter, EdsVoid * context) {
	EdsError err = EDS_ERR_OK;
	// do something
	return err;
}

bool MyCameraControl::setEventHandler() {

	//Create CameraController
	//m_camera_controller = new CameraController();
	//Create View Dialog
	//CCameraControlDlg view;

	//m_camera_controller->setCameraModel(m_camera_model);
	//m_camera_model->addObserver(&view);
	// Send Model Event to view	
	//view.setCameraController(m_camera_controller);


	m_camera_model.reset(new CameraModel(m_camera));
	m_camera_model->addObserver(this); //!!!!!

	m_camera_controller.reset(new CameraController());
	//m_camera_controller->setCameraModel(m_camera_model.get()); //!!!!
	m_camera_controller->setCameraModel(m_camera_model);

	EdsInt32 saveTarget = kEdsSaveTo_Both;

	// Set Object event handler
	if (m_err == EDS_ERR_OK){
		//m_err = EdsSetObjectEventHandler(m_camera, kEdsObjectEvent_All, CameraEventListener::handleObjectEvent, &saveTarget);
		m_err = EdsSetObjectEventHandler(m_camera, kEdsObjectEvent_All, CameraEventListener::handleObjectEvent, (EdsVoid *)m_camera_controller.get());
	}											   
	//Set Property event handler
	if (m_err == EDS_ERR_OK){
		m_err = EdsSetPropertyEventHandler(m_camera, kEdsPropertyEvent_All, CameraEventListener::handlePropertyEvent, (EdsVoid *)m_camera_controller.get());
	}
	//Set State event handler
	if (m_err == EDS_ERR_OK){
		m_err = EdsSetCameraStateEventHandler(m_camera, kEdsStateEvent_All, CameraEventListener::handleStateEvent, (EdsVoid *)m_camera_controller.get());
	}

	m_camera_controller->run(); //!!!!!

	if (m_err != EDS_ERR_OK) {
		return false;
	}
	return true;
}

void MyCameraControl::update(Observable* from, CameraEvent *e) {
	std::string event = e->getEvent();

	std::cout << "CameraEvent: " << event << std::endl;

	if (event == "DownloadComplete") {
		CameraEventListener::setPictureTaken(true);
		//toDo: signal to contoll app -> download finished
	}

}

void MyCameraControl::cameraModelFactory(EdsDeviceInfo deviceInfo){
	
	// if Legacy protocol.
	if (deviceInfo.deviceSubType == 0){
		m_camera_model.reset(new CameraModelLegacy(m_camera));
		//m_camera_model = new CameraModelLegacy(m_camera);
	}

	// PTP protocol.
	m_camera_model.reset(new CameraModel(m_camera));
	//m_camera_model = new CameraModel(m_camera);

}

EdsError MyCameraControl::getFirstCamera(){

	EdsCameraListRef cameraList = NULL;
	EdsUInt32 count = 0;

	// Get camera list 
	m_err = EdsGetCameraList(&cameraList);

	// Get number of cameras 
	if (m_err == EDS_ERR_OK)
	{
		m_err = EdsGetChildCount(cameraList, &count);
		if (count == 0){
			m_err = EDS_ERR_DEVICE_NOT_FOUND;
		}
	}
	// Get first camera retrieved 
	if (m_err == EDS_ERR_OK){
		m_err = EdsGetChildAtIndex(cameraList, 0, &m_camera);
	}

	// Release camera list 
	if (cameraList != NULL)
	{
		EdsRelease(cameraList);
		cameraList = NULL;
	}
	return m_err;
}

bool MyCameraControl::takePicture(){

	//m_err = EdsSendCommand(m_camera, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_Completely);
	//m_err = EdsSendCommand(m_camera, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_OFF);

	ActionEvent event("TakePicture");
	m_camera_controller->actionPerformed(event);

	if (m_err != EDS_ERR_OK) {
		std::cout << "Failed to capture an image." << std::endl;
		return false;
	}
	else {
		CameraEventListener::windowsMessageLoop();
		return true;
	}
}

void MyCameraControl::setDownloadFolder(std::string folder) {
	m_camera_controller->m_download_folder = folder;
}