#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ApplicationControll.h"

class ApplicationControll : public QMainWindow
{
	Q_OBJECT

public:
	ApplicationControll(QWidget *parent = Q_NULLPTR);

private:
	Ui::ApplicationControllClass ui;
};
