#include "ApplicationControll.h"
#include "../CameraControlApp/MyCameraControl.h"
#include <QtWidgets/QApplication>

#include <iostream>

int main(int argc, char *argv[])
{

	std::cout << "--------------------------" << std::endl;
	try {
	MyCameraControl cameraControl;
	if (cameraControl.initializeCamera()) {
		cameraControl.setDownloadFolder("./images");
		cameraControl.takePicture();
		cameraControl.deinitializeCamera();
	}
}
catch (...) {
	std::cout << "Shutdown error handeled." << std::endl;
}

	//QApplication a(argc, argv);
	//ApplicationControll w;
	//w.show();
	//return a.exec();
}
