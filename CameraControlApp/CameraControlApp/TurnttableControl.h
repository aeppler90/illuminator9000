#pragma once

#include "QtSerialPort/qserialport.h"
#include "QtCore/qtextstream.h"

class TurnttableControl
{
public:

	bool initialze();
	void deinitialze();
	void turn(float degree);
	TurnttableControl();
	~TurnttableControl();

private:
	QSerialPort m_port;
	QTextStream m_in{ stdin };
	QTextStream m_out{ stdout };
};

