#include "pch.h"
#include "TurnttableControl.h"

#include <iostream>

TurnttableControl::TurnttableControl()
{
}


TurnttableControl::~TurnttableControl()
{
}

bool TurnttableControl::initialze() {
	
	m_port.setPortName("COM8"); //if the arduino is not detected change the COM-Port
	m_port.setBaudRate(9600);
	m_port.setDataBits(QSerialPort::Data8);
	m_port.setParity(QSerialPort::NoParity);
	m_port.setStopBits(QSerialPort::OneStop);
	m_port.setFlowControl(QSerialPort::NoFlowControl);

	if (!m_port.open(QSerialPort::ReadWrite)) {
		//m_out << "Error opening serial port: " << m_port.errorString() << endl;
		return false;
	}

	return true;
}

void TurnttableControl::deinitialze() {

}

void TurnttableControl::turn(float degree) {

	QString qs;
	qs = "a" + QString::number(degree);
	QByteArray data(qs.toUtf8());
	m_port.write(data);

}
