
// Include Libraries
#include "StepperMotor.h"

#define STEPPER_PIN_STEP  7
#define STEPPER_PIN_DIR 6

#define stepperDelayTime  1000
// object initialization
StepperMotor stepper(STEPPER_PIN_STEP,STEPPER_PIN_DIR);


// define vars for testing menu
long steps = 0;
char c = ' ';

// Rotates the Turntable by degrees that are parsed over the serial port
void rotateTurntable(){

    steps = (int)Serial.parseFloat(SKIP_ALL)*36571.42/360.0;
    stepper.step(1, steps/2);  // move motor 1000 steps in one direction
    stepper.step(1, steps/2);  // move motor 1000 steps in one direction

    Serial.println("r");
    //delay(1000);            // short stop
    //stepper.step(0, steps/2);  // move motor 1000 steps in the other dirction
    //stepper.step(0, steps/2);  // move motor 1000 steps in the other dirction
    //delay(1000);
}

// Setup the essentials for your circuit to work. It runs first every time your circuit is powered with electricity.
void setup()
{
    // Setup Serial which is useful for debugging
    // Use the Serial Monitor to view printed messages
    Serial.begin(9600);
    while (!Serial) ; // wait for serial port to connect. Needed for native USB
    Serial.println("s");
    // enable the stepper motor, use .disable() to disable the motor
    stepper.enable();
    // set stepper motor speed by changing the delay value, the higher the delay the slower the motor will turn
    stepper.setStepDelay(stepperDelayTime);

}

// Main logic of your circuit. It defines the interaction between the components you selected. After setup, it runs over and over again, in an eternal loop.
void loop()
{
    c = ' ';
    // Waiting for Input from Serial Port
     while (Serial.available())
     {
        c = Serial.read();
     }

    if(c == 'a' && isAlpha(c)) {
        rotateTurntable();
    }
}
